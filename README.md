# Empty project

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/empty-project.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/empty-project.git
```
