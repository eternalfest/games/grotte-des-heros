package custom_msg;

import custom_msg.actions.CustomMsgAction;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import patchman.module.Run;
import ef.api.run.IRun;

@:build(patchman.Build.di())
class CustomMsg {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    private var run: Run;

    public var customTexts: Map<String, String> = [
        Obfu.raw("msg_400") => "*%username%*, vous venez de triompher de la Grotte des Héros. En entrant par cette porte, vous irez recevoir votre titre.",
        Obfu.raw("msg_404") => "Félicitations à vous, héros *%username%*, installez-vous sur votre trône pour immortaliser votre prouesse."
    ];


    public function new(run: Run) {
        this.run = run;

        this.actions = FrozenArray.of(
            (new CustomMsgAction(this): IAction)
        );

        var patches = [];

        this.patches = FrozenArray.from(patches);
    }

    public function customMsg(game: hf.mode.GameMode, id: String): Void {
        var getRun = this.run.getRun();
        game.attachPop('\n' + game.root.Tools.replace(customTexts[id], '%username%', getRun.user.displayName), false);
    }
}
