import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;
import better_script.BetterScript;
import custom_msg.CustomMsg;
import grotte.Actions;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    actions: Actions,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    customMsg: CustomMsg,
    game_params: GameParams,
    betterScript: BetterScript,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
