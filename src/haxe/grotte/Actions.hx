package grotte;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;
import grotte.actions.Turn;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var turn(default, null): IAction;

  public function new() {
    this.patches = FrozenArray.from(patches);
    this.turn = new Turn();
  }
}
