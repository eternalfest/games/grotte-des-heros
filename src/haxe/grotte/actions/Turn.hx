package grotte.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.Player;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class Turn implements IAction {
  public var name(default, null): String = Obfu.raw("turn");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    
    var game: GameMode = ctx.getGame();
    for(player in game.getPlayerList()) {
        player.dir = -1;
    }
      
    return false;
  }
}